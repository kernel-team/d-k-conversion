#!/bin/sh

set -eu

# svn2git misses many of the merges from sid to trunk
svn_rev="$(git log --pretty=%B $1 -1 | sed -n 's/^svn.* revision=//p')"
case "$svn_rev" in
    22925)
	extra=22910
	;;
    22336)
	extra=22333
	;;
    22322) # ???
	extra=22321
	;;
    21950)
	extra=21798
	;;
    21177)
	extra=21174
	;;
    21135)
	extra=21066
	;;
    21027)
	extra=21025
	;;
    21018)
	extra=21016
	;;
    20807)
	extra=20755
	;;
    20720)
	extra=20533
	;;
    20464)
	extra=20360
	;;
    20197) # ???
	extra=20193
	;;
    19666) # ???
	extra=19192
	;;
    18883)
	extra=18864
	;;
    18806)
	extra=18795
	;;
    18609)
	extra=18396
	;;
    17559)
	extra=17467
	;;
    *)
	exit 0
	;;
esac

git rev-list --all --grep="^svn.* revision=$extra\$" | head -1
