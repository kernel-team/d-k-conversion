#!/bin/bash

set -eu

svn_url="file://$(readlink -f svn)"
dir="$PWD"

rm -rf *.git
rm -f log-* gitlog-*

set -o pipefail
svn-all-fast-export \
	--identity-map=authors.txt \
	--rules=kernel.rules \
	--stats \
	--debug-rules \
	--add-metadata \
	svn 2>&1 | \
    tee svn-all-fast-export.log
set +o pipefail

move_tag() {
    local tag=$1
    local rev=$2
    local tag_commit="$(git rev-parse $tag)"
    git tag -d $tag
    GIT_COMMITTER_NAME="$(git log --pretty=%cn $tag_commit -1)" \
	GIT_COMMITTER_EMAIL="$(git log --pretty=%ce $tag_commit -1)" \
	GIT_COMMITTER_DATE="$(git log --pretty=%cd $tag_commit -1)" \
	git tag -m "$(git log --pretty=%B $tag_commit -1)" $tag $rev
}

pushd linux.git

# Fix up tags which were copied from the debian subdirectory.
# svn2git doesn't seem to have an option for this.
for tag in debian/2.6.16-2 debian/2.6.32-48squeeze{9,10}; do
    if git diff --quiet $tag $tag^:debian; then
	move_tag $tag $(git rev-parse $tag^)
    fi
done

# Fix up 2.6.26-14 tag which was copied from the parent directory.
# svn2git doesn't seem to have an option for this.
for tag_message in debian/2.6.26-14:'\[mips64\] Fix sign extend issue in llseek syscall'; do
    tag=${tag_message%%:*}
    message="${tag_message#*:}"
    rev="$(git rev-list --all --grep="$message")"
    if git diff --quiet $tag:linux-2.6 $rev; then
	move_tag $tag $rev
    fi
done

# Delete bogus tag - can't simply ignore it in kernel.rules as the
# correct tag was copied from it in svn.
for tag in debian/4; do
    git tag -d $tag
done

# Delete unwanted branches
git branch -D fs test

popd

pushd linux-latest.git

# Create branches which svn2git couldn't handle
git branch squeeze refs/tags/29

popd

pushd linux-tools.git

# Add missing merges
git filter-branch \
    --parent-filter "\
read parents; \
printf %s \"\$parents\"; \
set -- \$parents; \
if [ \$# -eq 2 ]; then \
    extra=\"\$($dir/linux-tools-add-parents.sh \$GIT_COMMIT)\"; \
    if [ -n \"\$extra\" ]; then \
        printf ' -p '; \
        map \$extra; \
    fi; \
fi" \
    --tag-name-filter cat \
    -- refs/heads/* refs/tags/debian/*
rm -r refs/original

popd

ret=0

for repo in *.git; do
    # Exclude the log files
    if [ -d "$repo" ]; then
	pushd "$repo"

	# Delete crap left behind by svn2git
	git tag -l 'backups/*' | xargs -r git tag -d
	rm -rf refs/backups

	# Clean up merges: svn only supports 2-way merges and any
	# extra parents are the result of silly mergeinfo
	git filter-branch \
	    --parent-filter 'read dummy1 parent1 dummy2 parent2 rest; echo $dummy1 $parent1 $dummy2 $parent2' \
	    --tag-name-filter cat \
	    -- $(find refs -type f)

	# Check that all branches and tags have a common ancestor
	root=
	for ref in $(find refs -type f); do
	    if [ -z "$root" ]; then
		# Hope the first one is good!
		root=$(git rev-list $ref | tail -1)
	    else
		if ! git merge-base --is-ancestor $root $ref; then
		    echo >&2 "W: unattached ref in $repo: $ref"
		    ret=1
		fi
	    fi
	done

	# Check that it is the *only* root
	echo >&2 "I: checking for extra roots"
	for rev in $(git rev-list --all --max-parents=0); do
	    if [ $rev != $root ]; then
		echo >&2 "W: extra root in $repo: $rev"
		ret=1
	    fi
	done

	# Check that all tags came from the right directory
	echo >&2 "I: checking tags"
	while read tag; do
	    if ! git show $tag:debian >/dev/null 2>&1; then
		echo >&2 "W: tag $tag in $repo came from the wrong directory"
		ret=1
	    fi
	done < <(git tag -l)

	# Now check that all refs match (except in kernel-team.git which
	# is a new thing and doesn't match any directory in svn)
	test $repo = kernel-team.git || \
	for ref in $(find refs -type f); do
	    echo >&2 "I: comparing svn and git contents for $ref"
	    git_export=$(mktemp -d)
	    svn_export=$(mktemp -d)
	    svn_info="$(git log --pretty=%B $ref -1 | grep ^svn | tail -1)"
	    svn_rev="${svn_info##* revision=}"
	    svn_dir="${svn_info#* path=}"
	    svn_dir="${svn_dir%%;*}"
	    git archive --format=tar $ref | tar -C $git_export -x
	    svn export --force "$svn_url/$svn_dir@$svn_rev" $svn_export >/dev/null
	    left=$svn_export
	    right=$git_export
	    if ! [ -d $svn_export/debian ]; then
		if [ -d $svn_export/${repo%.git} ]; then
		    left=$svn_export/${repo%.git}
		else
		    right=$git_export/debian
		fi
	    fi
	    diff -urN $left $right || ret=1
	    rm -rf $git_export $svn_export
	done

	popd
    fi
done

exit $ret	# That's all we're doing for now

# Create tags for Debian orig versions.
# Sort them in reverse order so N.dfsg.1.orig.tar.gz comes after N.orig.tar.gz
for orig_tarball in $(ls linux{-2.6,-2.6.24,}_*.orig.tar.[gx]z | tac); do
    orig_ver=${orig_tarball%.orig.tar.[gx]z}
    orig_ver=${orig_ver#*_}
    tag=upstream/${orig_ver/\~/_}

    if GIT_DIR=linux-upstream/.git git rev-list $tag -1 >/dev/null 2>&1; then
	continue
    fi

    if [ ${orig_ver%.dfsg.*} = $orig_ver ]; then
	# Convert old and new notations for pre-releases
	parent_tag=v${orig_ver#*+}
	parent_tag=${parent_tag/\~/-}
	# Never include .0 as last component
	parent_tag=${parent_tag%.0}
	parent_tag=${parent_tag/.0-/-}
    else
	# A very few upstream versions had multiple Debian orig
	# versions with more non-free bits removed
	parent_tag=upstream/${orig_ver%.dfsg.*}
	dfsg_rev=${orig_ver##*.dfsg.}
	if [ $dfsg_rev -gt 1 ]; then
	    parent_tag=$parent_tag.dfsg.$((dfsg_rev - 1))
	fi
    fi

    pushd linux-upstream
    if [ "$parent_tag" = v2.6.11 ]; then
	parents=
    else
	parents="-p $(git rev-list $parent_tag -1)"
    fi
    for entry in * .*; do
	case "$entry" in
	    . | .. | .git)
		;;
	    * | .*)
		rm -rf "$entry"
		;;
	esac
    done
    tar -xaf "../$orig_tarball" --strip-components 1
    git add --all -f .
    export GIT_AUTHOR_NAME='Debian kernel team'
    export GIT_AUTHOR_EMAIL='debian-kernel@lists.debian.org'
    export GIT_AUTHOR_DATE="$(stat -c %Y "../$orig_tarball") +0000"
    commit="$(echo "Debian orig version $orig_ver" | git commit-tree $(git write-tree) $parents)"
    git tag $tag $commit
    unset GIT_AUTHOR_NAME GIT_AUTHOR_EMAIL GIT_AUTHOR_DATE
    popd
done

# Clean up
pushd linux-upstream
git reset --hard
popd

# Rewrite linux branches with upstream merged in
dir="$PWD"
pushd linux.git
git config remote.upstream.url >/dev/null || \
    git remote add upstream "$dir/linux-upstream"
git fetch --tags upstream
git filter-branch --parent-filter "'$dir/merge-orig-parent.sh'" \
    --tree-filter "'$dir/merge-orig-tree.sh' '$dir' 'linux{,-2.6,-2.6.24}'" \
    --tag-name-filter cat \
    -- etch{,nhalf}{,-security} lenny{,-security} master sid {squeeze,wheezy,jessie}{,-backports,-security}
popd

# TODO: Do we also want to do this for linux-tools?
