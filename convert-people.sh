#!/bin/bash

set -eu

svn_url="file://$(readlink -f svn)"
dir="$PWD"

rm -rf *.git
rm -f log-* gitlog-*

set -o pipefail
svn-all-fast-export \
	--identity-map=authors.txt \
	--rules=kernel-people.rules \
	--stats \
	--debug-rules \
	--add-metadata \
	svn 2>&1 | \
    tee svn-all-fast-export.log
set +o pipefail

# Move into new subdirectories
cd kernel-team.git
git filter-branch --tree-filter 'set -- * && mkdir -p scripts/benh && mv -t scripts/benh "$@"' -- benh-scripts
rm -r refs/original
git filter-branch --tree-filter 'set -- * && mkdir -p utils/kconfigeditor2 && mv -t utils/kconfigeditor2 "$@"' -- kconfigeditor2
rm -r refs/original
