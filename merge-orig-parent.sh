#!/bin/bash

set -exu

read parents
set -- $parents

# Fix up some cases svn2git gets wrong
case $GIT_COMMIT in
    ff77692ecab82d4f02632f78abaefeafc2fec4e2)
        # parent was not noted
	echo -p 1be89bc8dd2ffc8dc43bb2bd4158f7c1a1582f56
	exit 0
	;;
    3ee2c68c7e81494f05604a8b725661ec03efa9af | \
    1dc1f2df35dd14d4228f5102d4c256b0c14f3ffc | \
    fb34291f369fcfaea16e2c3dec9e80fbf4493cea | \
    8951159725a4ce0fd4cbd272d49f095dfcde0be0 | \
    f1022ff757e574165b5c9d1aa039cf35c2d622db | \
    e08f90e2fdf0118abe6a00543c35df9cb8fb5df4 | \
    696123e59dd63923d73c1ec7a6f7a3bd71a8f295 | \
    8ad7a1f25eaa34c5b402b0d89b28dde0904f05a4 | \
    77e5e9303326d6aa1ec930dd59473df074591ba5 | \
    31849dbbddb7dd1db402ddd53dfc4683473c8d6c | \
    dbba09d1c77da16b7909b95d66c73795ffe7d214 | \
    c2ef6aa2187c6c26c689408172fc0c9d241deabe | \
    929891bb000805db7862427423b0fb6e4ff3c3f9 | \
    1086f5b91ff4947edee64c2ad79655913e8568d0 | \
    69d903c5f13971e35fc02f6139cbe9c876ca3841 | \
    2edb7cffd1fcb8f6bfd445473d72e13073f996b2 | \
    2429876af93e3f0be5ff32d105d103ed77e99f6c | \
    0fc148af93429cdc95bcd9f7b8c36eb4862ead61 | \
    cfaa922ec16ddc5d8698ad47644c4b00e31a0f0f | \
    ba3f64da95c6a0bbfe275e8145045bafe2a9442c | \
    7772f1d85f59399fc207224c9c1bb1a0f455cb9d | \
    10ea9e506e5b972120b846acf2104d8bde052d57 | \
    e00784dd4c18e8563a0b7f7edb2f546c604434b1 | \
    6da0e3a3126a604dadfb67ef2ca810c02c316a88 | \
    352369edc9eb087b6965649879f79ae8e8e6f5b0 | \
    8e7dddbd2bc5f7dbca4a7453e64900fb4e934fb0 | \
    2426cf8872f31ed3a9a3354827b9ad931c563969 | \
    2cbdf9396a19acc34c4ad15cd1e97085b8478101 | \
    baf10a8235265dac0f9ae2761f101f684fad1dbc | \
    b03ae8ef8faa788f0fd65622f3ad170cfb4cfffc | \
    d1bb2348d3c8206698f0233684986489d2af5a38 | \
    905686915a36b0d851e162cdfa443b629e2763d6 | \
    dbfe910291b6f2b33c7aba3698f08bc6657392c1 | \
    34ae04fd8598e74f6186346995fb53d81abe4c7c | \
    c7661a957053463b02fe0602400883dbb8e07e48 | \
    e76e5da360ba268ae335d2561579dec9e52e420e | \
    78cd3efb336311ab3cfd6b8c44980094b58747c4 | \
    838be800b0bd67542b638a6c426a616bfc1ec8a3 | \
    39aa788d4773e73eff9686f3e623aa4836480420 | \
    807eb010ecfb853569129f0f3e60bc7955095343 | \
    2e4a53b40d4d4b6fe83e4fb94fab8a030b8c35a8 | \
    8f57e657d84e4c8ff71c26b3def3b13db430c3df | \
    50494011e832493278c47b5053215fd3697522fd | \
    d7dc46d76fbabd4a8b3697d30032fa4ccd0963ae | \
    6bedc9e05729847e6328d7004475fb45b871e85c | \
    f0dc6795332d4818051cb2d423179553ac7244ac | \
    edf2b8bfa0d40e50625b609f3dba20aead067d48 | \
    d431bd357400e7e0ec6303c1a7aec69f67b0e522 | \
    a9510589d6a68736342b80eb23ae9a9d9c2c50bb | \
    0b9950411527721bd2864a2525b5132763164712 | \
    e85eb87cb75233a1a70cb43ed7cba859d9e1a04b | \
    a42f2ac439bbbc47528eb8bd442beb5fbb5b9078 | \
    d529e4d5c4e46b86e8d4ff25b94380537060e095 | \
    4ae7f0f61178567185ec58b4aa054400c44811b2 | \
    78be1c7822842fca1df612ef51333c2ee308a91c | \
    360cea66afa4ecc6eb9b107ffb8270e3d4a22703 | \
    e0aa48011b7587b9ab5ca5bb207f18f06d7d2e32 | \
    ff46cb9d5f8e4c57d26a844609b85c1f2a19c777 | \
    8a6e9d72a9af38ffdd5abcfcb5eff46d46661506 | \
    dfb2495bff0000324cd9a197347ca5e5e73d8378 | \
    d9843a9516a159ac61d6a0d6d477858590c53d70 | \
    ea7f801e054f87a1090844e201945d486ee82466 | \
    ac2a4722ba32a32f118bc8f1c42e2b5153e84ed9 | \
    17377a12863587ba83c77b0a94afeaa3bfe48834 | \
    10aa7d24b2b7630d9441e0cd4386521d31aaa74d | \
    a3c04c0cc584723a754b1aaa089cf461f46ebd2e | \
    80a9468a5f079362787617c5e89c84f286f38414 | \
    615bc87e56c314940e9ff4c7b5c8b202cd057f44 | \
    cad2d5da4eae5b4e7cc0ec8b3a94c963c81e710a | \
    30497a7527b5c12087a23db6196afd599cd5294b | \
    81eb83c015ccc9aa16f7d6fcba350e83e8582a3d | \
    9bf5273afe58d07bc30e18864df0232a3ea40f56 | \
    df431fa7078b3515953e5abdf456fd4f49d91658 | \
    8da91b09b865091c2c0aa5c0734bbcd8581a06e7 | \
    11733bd778e1b73feb5c504016ba6de2bfed9042 | \
    bed714742d20291463bb5bcc6134d0e092d16425 | \
    bdb22c41e54d3b344b897d8a870f6d791dfd81ed | \
    00d90e133cc73dab46135b7dcf11095c916b161b | \
    269ee9b0e712515542b5a63467f7648267144544 | \
    293667a540252e4c94d44f69b96e4004a69e847d | \
    dd35c2a4dbb6a8cde5314d499d33aad1a16f9c7b | \
    dd9c7b2ca366e68b620e2e4178e0e0aec671318f | \
    1298b2fb3fd20ef1182791db9de5d190a2ad989d | \
    a0b42fb98fa119eac7625870cc194faecf362194 | \
    576147c23cf61a989ec954633c1fa06671dc2ee0 | \
    7a7693d772226835c9f303f2413b84e4dddaf838 | \
    2b5265f3ed85b45e8c957e58b2279cd3dbcc579b | \
    739e6b46781ab8ca8517c1e54b237ced8f6a9cd0 | \
    c454287a85916097477c31fdf8c8b83712107d3f | \
    2b4ef4762d92f94de0c762973f84ed88b9753bc9 | \
    7cf1f49a8edc4c14ade8a307c58eb93598dfee4f | \
    413885f0ec47772a56819fb5bbac43eba1f1e038 | \
    87a31465b8f05067aed2b26fb93c2ebd5cdd39b2 | \
    5b9f37cf7cdfe7388b98dc4fd9f74f7984c7fac0 | \
    4a3b9aae32b96c0a4ef96fe19dce6a402203c7d5 | \
    d8508a666c67632a3f2b8b9d4f7c26ce2fe62565 | \
    fe9a66011d14e9fa73eb52fa1a713e5ce13dde69 | \
    8ee973a4bce267ccd02f85c9a0d804ead4e5dac1 | \
    25862e47e8faa0990f73376cf59041c2e3df7f2e | \
    e9bc5cde2ab880b1ec8609bf4e991208e7e11c81 | \
    9b1d3a2b1a046c013b9fd86534fddfa54ddbb6fd | \
    a3213910acd2a9740dec4b24418a952cca34fa9c | \
    8d3d6fb70c7894281b3967f23ee8ab0bb776a44c | \
    8ea23380801faa9b85502b3383fe31ae140576b8 | \
    87bd7e45cf72ebcdfb49752d1ecdc8f779cd085d | \
    7232afbc7eb483aafdbcfcbf6a20d3edb79d1842 | \
    a9e7f76514f8da8ca5241ca52122aea9e03e6192 | \
    2d2d961a29e2778b39768dd9ce9a0db25cf9f626 | \
    0214d36ed6e8864c7b91c15858b9ae8f09ed7eb5 | \
    0615a24134e7dee8440e7e9def86371abb3d3b4d | \
    de9cee9be3462f36f26d2d69cf10f41cd9815b6d | \
    db62e17d5f147c29353fd23cb2010ae203b5ac48 | \
    d7956fc8d91c6f3a2b6451d422351336c220558f | \
    7cc7d212474bf2712aefaed5925fb5cdac5dc389 | \
    e963625392c61cf670cc1948dacfb8d92979b0b9 | \
    45795621bb650e0e63b6d0f1c2d517179dc073be | \
    947a53c28c94a64a48c232a66470117e12a1d0e3 | \
    e93a23656bd149d2a02ff2613ad496755f1a7c02 | \
    1a380861a3f85599a9d8756acdf1582097a7be92 | \
    527c3603efc0b4f63564f753a73ff33249835a36 | \
    999a3f292616625fc6d8aa9b56596adc70d5cb84 | \
    0a5e44fbe706732e4fe7b5f6560e50eb4f6dadad | \
    df8e02d837c13b4cd5c4a734aba85bc71be827b4 | \
    77e5e9303326d6aa1ec930dd59473df074591ba5 | \
    0225e66658f3af3dc808b91ce9162cd8fbdfddc6 | \
    9749411266bdec19a70304312c176e4cf9c3d62b | \
    35089892c9f71115a6d1e6fb204759830ee92e7f | \
    9362aae2d53bb41340404ac833a037e01efc16ec | \
    282a6f09ff15e44d1908a913e4d8a1328fad9beb | \
    a061ef1941b180276925c560a52064d1d7712289 | \
    666d921c3ebaa1e5abd7a708638555e771710cc5 | \
    0f0f4dee975d6583b531c74dee666e8e082b833f | \
    147c03ab1631ae364f0e3578abfd81849d19c0c5 | \
    1a22df2e0e302ca80541c16965cdeaf1342db75e | \
    db58c0a95701f3e2c2ca84992611286431908f86 | \
    2d7ad983fa70775a16866269813971df2d4867a7 | \
    d4d03c8520d00206403efbc9cd369e5c548eb78f | \
    4d92cc8ec5379b2093ed51ffeb6bc8708f4f9c86 | \
    573cc45028b18ab4dcb7cebafe58f2eedf6a5acd | \
    9d6c4796bfcbed90282d6f2be900d7ce19c5dfa8 | \
    b327a3d0a03001048abf949bac9ee1b71293cee4 | \
    eb68cc2bffe67168a6e280e6729f787d83c692d6 | \
    2565d02214d8d76814068768811d67a709a45f56 | \
    1148a6c42c528a6002f30a8418f99793a89d80f5 | \
    2063ec9ff5ec5e20e8453642aa2278cb46d218f1 | \
    383f8bf2b526bec9b134eb26d8335a49e0b2d12b | \
    bc4b5b7f0ff5531b5d9c835772cbb6a42d2641c8 | \
    3740be7fa07d21d5a75907993d0f9f1df91116fc | \
    9900c079e23058af0efb94b3e6ec04ec09679642 | \
    1e75fe1c2298a2e55d15e9d5b2a6e2bbd297a9aa | \
    3e067f07f71a1ca4124c8bf17739267571183cc5 | \
    6bb0c871b6e18711837d5e8a9184f8823d3fece3 | \
    6a2bae185c7509b4a674387ce5a943667e5a49a1 | \
    2809d3670b72b94739f508fbdec75b0c1033382c | \
    b2d7b700df495c5c707264b6929e3dec229eb5c6 | \
    38f80e0193a7d7dd146210f8f11cd07d71b7c2cb | \
    a6932c0c147a9bb405d6259a3bee578ddec0b6dc | \
    79e2e8593ee55422c431e04c1dba7687293b494b | \
    086a3bcb3395d7e99fc811b246611767646af4bb | \
    29d592bb9cccd446971ee1cfc5b063bb29df969f | \
    044847e9615f97566b6b71087983dbf7130ce029 | \
    e85eb87cb75233a1a70cb43ed7cba859d9e1a04b | \
    f7c4e4158fbe1c188767e466449c54ba9f7fc259)
        # cross-branch copies, more like cherry-picks than merges
	echo -p $2
	exit 0
	;;
esac

# Skip existing merges - those will always leave us with the same
# orig version as one of the parents
if [ "$#" -le 2 ]; then
    changelog="$(mktemp)"
    trap 'rm "$changelog"' EXIT

    get_orig_ver() {
	local ver
        # If there is anything outside the debian directory then *do not*
        # import orig as it may conflict
	if [ "$(git show --raw $1: | wc -l)" -eq 3 ]; then
            # dpkg-parsechangelog reads the file twice so we can't use a pipe
	    git show $1:debian/changelog >"$changelog"
	    if ver="$(dpkg-parsechangelog -l"$changelog" --show-field Version)"; then
		echo "${ver%-*}"
	    fi
	fi
    }

    orig_ver="$(get_orig_ver $GIT_COMMIT)"
    if [ -n "$orig_ver" ]; then
        # If this is not the initial commit, get the parent's orig ver
	if [ "$#" -eq 2 ]; then
	    prev_orig_ver="$(get_orig_ver $2)"
	else
	    prev_orig_ver=
	fi

	if [ "$orig_ver" != "$prev_orig_ver" ]; then
            # Orig version was bumped; add the 'Debian orig' version
	    # (after removing non-free bits) as parent... if we can find it
            if orig_commit="$(git rev-list upstream/${orig_ver/~/-} -1)"; then
		parents="$parents -p $orig_commit"
	    fi
	fi
    fi
fi

echo $parents
