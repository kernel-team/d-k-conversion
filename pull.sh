#!/bin/bash

set -exu

rsync --progress --archive --hard-links alioth.debian.org:/srv/svn.debian.org/svn/kernel/ svn/

exit 0	# That's all we're doing for now

# Prepare a git repo with all Linus and stable tags.
# This must not be bare as we will import orig tarballs below.
if [ -d ../linux/.git ]; then
    reference='--reference ../linux'
else
    reference=''
fi
if [ ! -d linux-upstream ]; then
    git clone $reference \
	git://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git \
	linux-upstream
    pushd linux-upstream
else
    pushd linux-upstream
    git remote update origin
fi
git config remote.stable.url >/dev/null || \
    git remote add stable \
    git://git.kernel.org/pub/scm/linux/kernel/git/stable/linux-stable.git
git remote update stable
popd

# Get all the released orig tarballs (in fact, complete source packages) for linux.
for src in linux-2.6 linux-2.6.24 linux; do
    debsnap -d . -v $src
done
wget -c http://snapshot.debian.org/archive/debian/20050327T000000Z/pool/main/k/kernel-source-2.6.11/kernel-source-2.6.11_2.6.11.orig.tar.gz
ln -s kernel-source-2.6.11_2.6.11.orig.tar.gz linux-2.6_2.6.11.orig.tar.gz

# TODO: Do we also want to do this for linux-tools?
