#!/bin/bash

set -exu
shopt -s nullglob

# If there is anything outside the debian directory then *do not*
# import orig as it may conflict
if [ "$(ls -a | wc -l)" -ne 3 ]; then
    exit 0
fi

orig_dir="$1"
package_names="$2"

# Try to find orig tarball based on changelog
if ver="$(dpkg-parsechangelog --show-field Version)"; then
    orig_ver="${ver%-*}"
    orig_tarball="$(eval echo "$orig_dir"/${package_names}_"$orig_ver".orig.tar.[gx]z)"
    if [ -n "$orig_tarball" ]; then
	tar -xaf "$orig_tarball" --strip-components 1
	exit 0
    fi
fi

# Fall back to copying orig from first parent
parent_commit="$(git show --pretty=%P --no-patch $GIT_COMMIT)"
parent_commit="${parent_commit%% *}"
git archive --format=tar $parent_commit | tar -x --exclude 'debian/*'
